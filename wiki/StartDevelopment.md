# 4. How to start development with the Hubs-Injection-Server.  

In this section we will learn how to boot the modified custom Hubs Client and the injection server and how to validate the stack is working as it should by building a simple hello world exemple. We will start by deploying our stack on our local environment. 

## Step 1. Run the injection server  
As previously shown, from a terminal, go back to where your injection server code resides.  
If your server is not already running: From the root of the project, run `node index.js` . 

## Step 2. Run your Hubs Client
Once the injection server is running, go ahead and fire up your custom Hubs Client. From the root of the Hubs Custom Client project, run `npm run dev`. 

## Step 3. Check connectivity  
Open up Firefox, go to https://hubs.local:8080 and validate your hubs client is running properly and create a new scene. Open up the web developper console and have a look if the Hubs Client properly injected JS code from the Injection Server. 
You should see a line that says:  
- `injecting https://api.hubs.local:8557/injection/fileMergedX.js`    
And some more generic logs coming from our socketIO connections. : 
- `IO connected event. clientId = XXX`
- `received room message HI from room`  

If you see logs along those lines with no Security Errors or Cross Domain Errors, you are ready to go.  To the contrary, if you get security errors, it usualy should be issues with the self signed certificates not being valid. In that case, you should:  
- Try to go to: https://api.hubs.local:8557 and see if you can set the self signed certificates as valid and reload the Hubs Client room page. 
- If that fails, deleting the Injection Server self signed certificates, rebooting the Injection Server and validating the new certificates should do the trick.  

Once you get evrything running properly, we can start looking at how to start development with the Injection Server.  

## Step 4. Understanding specific rooms code injection and generic injection.  

While working in Mozilla Hubs, one needs to understand that each room created is assigned a hubId. You can usually see it in the browser address bar. For exemple, in the following local url `https://hubs.local:8080/hub.html?hub_id=KCwBe9T` we can clearly see it passed inside the querystring: `hub_id=KCwBe9T` . Some other times, you might see it in some other notation form such as: `https://whateverhubsdomain.com/gg6CydS/aRoomName` where the hubId is: `gg6CydS`. You will also encounter scenes where the hubId is hidden and not part of the url. That hubId is the essential part of being able to segregate code from one room to another.  

If you go back and have a look at the injectJS function we have added to the Hubs Client, you will see how we use the hubId to build a query to the Injection Server. Once the injection server receives a proper query, it will check if it has any specific code to inject for that hubId and deliver it as a bundled up .js file to the Hubs Client that will dynamicly insert it inside the DOM. If the Injection Server finds NO code for that specific room (hubId), it will default to sending code that we call the `generic` "room".  


## Step 5. Injecting code for a specific room and creating the Hello World Exemple.   

- Create a new room. 
- Find it's hubId. 
- Open up your Injection Server project and open up the rooms folder located at static/rooms .
- Create a new folder and name it accordingly to your hubId. 
- Inside your newly created folder, create a new file called `configs.json`. 
- Paste the following inside your file (At this point, you can also copy-paste the configs.json file located inside the generic room folder ).  
```
{
    "modules": {
        "client":
        [
            {"id":"satutils", "url":"/lib/satutils.js"},
            {"id":"utils", "url":"/lib/utils.js"},
        ],
        "server":
        [
            
        ]
    }  
}
```  
Note that all configs should start with the 2 libraries included above (satutils.js and utils.js).    
- Now we need to add some room specific code for your room. We will start by adding a console.log message to validate everything is working properly. Go into the static/lib and create a new .js file called: `consoleHelloWorld.js` and add the following :  
```
setTimeout(()=> {
    console.log(`******** HELLO WORLD FROM INJECTED JS ****** from hubId: ${SAT.Utils.getHubID()}`);
}, 3000);
```  
- Save that file and go back the `configs.json` file. We will now add a link to our `consoleHelloWorld.js` file.  
Under modules.client, add a new object to the library array. The object should take the following form:  `{"id":"consoleHelloWorld", "url":"/lib/consoleHelloWorld.js"}`. The value for the `id` key here is not really important for the moment, so you can use whatever your heart desires. What's important for us is the value for the `url` key.  
The whole `configs.json` file should now be:  
```
{
    "modules": {
        "client":
        [
            {"id":"satutils", "url":"/lib/satutils.js"},
            {"id":"utils", "url":"/lib/utils.js"},
            {"id":"consoleHelloWorld", "url":"/lib/consoleHelloWorld.js"}
        ],
        "server":
        [
            
        ]
    }  
}

```   


- Save your work and reload your Hubs Client room.  
Everything worked properly if you get the hello world log message in your browser.  
Congratulations, you have now deployed your first code injection inside a Mozilla Hubs Client. If you haven't noticed yet, for most changes you will do here, there are no need to reload the Injection Server application.  We really enjoy that, we hope you will too.  

**GENERIC**  
If you ever need to quickly inject code into your rooms without going through the creation of folders, you can use the static/rooms/generic `configs.json` to build and test your work. Remember to clean it up once your done if your code needs to run on a specific hubId.  
Once more, the `generic` room folder is for injecting in every room (hubId) that is not specified. If your room is setup properly, the `generic` fallback logic will be ignored.  



| Previous Page | Home | Next Page |
| ------ | ------ | ------ |
| [SettingUpAdvancedHubsCloudAdminOptions](./SettingUpAdvancedHubsCloudAdminOptions.md) | [index](../README.md) | [How to create injected components for Mozilla Hubs.](./wiki/HowToCreateInjectedComponent.md) |
