# 1. Setting up the Hubs Injection Server

## Injection server

As earlier mentioned, this document is designed to get you through the details on how our Hubs Injection Server works and how to set it up. 

The main objective behind this module is to very lightly modify the Mozilla Hubs Client's code and to allow quick component injection on a per-room basis without having to rebuild and/or redeploy the client.  
 
This works well for us, and we use it in production but we would not qualify this, in any means, as production ready, as it has not been tested at a massive scale. 

We have gained a lot of traction and velocity in our production flow by using this method, we hope it will inspire some nice folks from the community to use it and modify it as they see fit.

One last point before we get started with the nitty gritty on how to install and use this.  Injecting javascript at runtime in any web page is often considered a **dangerous and hazardous** method on the **security** front. Even though cross-domain policies are quite strict for the Mozilla Hubs client, be aware that you need to check your Ps and Qs on what you are side-loading into the client and pay extra-attention at being a nice "roommate".  

## Stack and dependencies  

The Injection server is a NodeJS project, built in pure javascript. You'll need NodeJS obviously.
The only dependency that is not installed along with the project (npm install) is MongoDB.  
Obviously, you'll need git and NodeJS installed before doing anything, but for simpplicty's sake, we will not refere anymore to those two as stack dependencies.  

**STEP 1: INSTALL MONGODB**  
Your first step to get this running properly is to install MongoDB if it is not already running on your development environment.  
[Get it here](https://docs.mongodb.com/manual/installation/)

We use MongoDB for storing states on components because the Hubs "pinnable/pin" option, while a great tool, did not work for us in a lot of use cases we were required to build. Our MongoDB usage is quite simple and limited but works very well. The storage method could easily be swapped for something else.  

If you do not need to use savable states, we will show you later on how you can "comment-out" this feature, therefore bypassing the need for a MongoDB installation.  

Now let's get on with the installation:  

**STEP 2: CLONE THIS PROJECT ON YOUR DEVELOPMENT BOX**  
git clone git@gitlab.com:sat-mtl/satellite/hubs-injection-server.git  

**STEP 3: INSTALL DEPENDENCIES** 
In your terminal/shell of choice, browse to the folder where you cloned the project and simply:  
`npm install`. 

**STEP 4: CREATE A .ENV FILE AT THE ROOT OF THE PROJECT** 
![envVars1](./images/envVars1.png)


And proceed to add the following lines (variables). 
```
BASE_URI = http://localhost:8557 
HUB_URI = https://hubs.local:8080  
DB_HOST = mongodb://localhost/hubsinjectiondb
```  

The first two environment variables need to reflect where both, the injection server and the Hubs client are residing. This setup is meant for local development where both, the client and the injection server, are running on your computer.  

**BASE_URI** is the location path of the injection server.  
**HUB_URI** is the location path of your custom Hubs Client.  
**DB_HOST** is the mongoDB location path. It should resolves with localhost as a URI on your computer and on any linux server solution. There is no need touch this unless you are thinking of scaling up with multiple DB servers, in most cases, this stays as it is. You can modify the db name at your liking (ie:  hubsinjectiondb ), just use smallercase letters and no special characters.  

As of September 2021, security features were added to the remote Mozilla Stack that prevents from running a custom client on localhost:8080. You will need to set up hubs local hostname (hubs.local) as indicated in the following link : [https://github.com/mozilla/reticulum#1-setup-the-hubslocal-hostname](https://github.com/mozilla/reticulum#1-setup-the-hubslocal-hostname) .  

Using the hubs.local hostname for your Hubs Client without changing anything else will make the stack work only in Firefox. That's because Firefox is a little bit more forgiving then the other browsers when it comes to secured domains communicating with domains that are not secured. In our case, the Hubs Client is now running under https but the injection server is not.   

**Setting up self signed certificates for the injection server running locally.**  
By "securing" our local injection server, we will now be able to now boot up the Mozilla Hubs Client in Firefox and Safari without getting security errors. Here are the steps to take in order to run the injection server under https.  
- Add a new local host name (like we did for the Hubs Client above) for your injection server and call it: **api.hubs.local** and target: **127.0.0.1** .  
- Modify the **BASE_URI** you have previously inserted to the follwoing value : **https://api.hubs.local:8557** ( **BASE_URI = https://api.hubs.local:8557**)  
- Add a new .env variable called **USE_LOCAL_SSL_CERTIFICATES** and set its value to **1** ( **USE_LOCAL_SSL_CERTIFICATES = 1** )
- Add a new .env variable called **REGEN_LOCAL_SSL_CERTIFICATES** and set its value to **0**. ( **REGEN_LOCAL_SSL_CERTIFICATES = 0** )
- Add a new .env variable called **LOCAL_HOSTNAME** and set its value accordingly the new hostanme we just added for the injection server, in our case : **LOCAL_HOSTNAME = api.hubs.local**  

Your .env file should now have the following:  
```
BASE_URI = https://api.hubs.local:8557
HUB_URI = https://hubs.local:8080
DB_HOST = mongodb://localhost/hubsinjectiondb
USE_LOCAL_SSL_CERTIFICATES = 1
REGEN_LOCAL_SSL_CERTIFICATES = 0;
LOCAL_HOSTNAME = api.hubs.local
```  
- Restart your server 
- Check if your certificates have been created, a new folder called certs should appear at the root of your project with certificate files inside.  
- Open up Safari or Firefox and type in the location of our now "secured" local environment of the injection server: **https://api.hubs.local:8557**  
- The browser will pop up some sort of warning to let you know that there is potential danger ahead... bypass this and allow the connection to the site.  
- Once you approved the certificate we will now be ready to start the custom Hubs Client and test the connectivity with the Injection Server.  

**WAIT, BUT WHAT ABOUT CHROME ?**  
Well, that's something a whole lot of people are asking with Chrome and development on localhost with self signed certificates. We are still looking for a proper way to get this going. There are a few techniques you will find on the web, but for us, nothing very conclusive so far as long as getting our Mozilla Hubs stack running. 


**.ENV FILE SETUP FOR AWS**

A standard setup on a Hubs Cloud server running on AWS could look like this: 

```
BASE_URI = https://yourInjectionServerSubDomain.yourHubsCloudDomain
HUB_URI = https://yourHubsCloudDomain
DB_HOST = mongodb://localhost/hubsinjectiondb
```

Beware of trailing port numbers when deploying on AWS or on other cloud solutions. Port numbers are usually only present when working on local environments.  


**STEP 5: START THE SERVER** 
If not already running, go to your terminal/shell of choice, browse to the folder where you cloned the project (chances are you are already there) and type the following to start: 

`node index.js`. 

If all goes well, you should get a message that the Injection Server is currently running on port 8557. 

![Screen_Shot_2021-05-24_at_2.48.27_PM](https://gitlab.com/sat-mtl/satellite/hubs-injection-server/-/wikis/uploads/dcf4d2b34ddd7998d33814acf4ea77c4/Screen_Shot_2021-05-24_at_2.48.27_PM.png). 

The injection server is now running in a very minimal, but functional state. Our next endeavour will be to modify the Mozilla Hubs Client in order to allow and receive code injected by the Injection Server.  

| Home | Next Page |
| ------ | ------ |
| [index](../README.md) | [Setting Up the Moziila Hubs Client](SettingUpTheMozillaHubsCustomClient.md ) |
