# 7. How to save states and values to a database.  
While building our stack, we've found ourselves needing an easy way to store assets and component states that would persists through time.  
To fill that need, we have put together a small API, accessible from anywhere in Hubs where you can store and retrieve assets.  
It is still in a proof of concept state, but we thought sharing it here might make some folks happy or might engage some of you to reflect on this.   


**SAVING TO DB**  
Use the Promise based `SAT.Utils.store.save` method.  
The save method resolves a promise on success and rejects it on error.  

There are three main "types" of objects we can save.  
- component  
The component store is meant to save data stemming from components in a particular room. Very usefull to store component states for your hubs experience. Keep in mind, it's stored based on hubId and componentId.  
`SAT.Utils.store.save("component", componentId, dataObjectToSave)`  

- generic-element  
The generic-element store is meant to save any kind of data from a particular hubId (room). You need to give a key (objectId) to the object you save so we can retrieve it. This is stored based on hubId and objectId.    
`SAT.Utils.store.save("generic-element", objectId, dataObjectToSave)`  

- global-generic-element  
The global-generic-element store is meant to save any kind of data from anywhere in your hubs client (any room). You need to give a key (objectId) to the object you save so we can retrieve it. This is stored based uniquely on objectId.   
`SAT.Utils.store.save("global-generic-element", objectId, dataObjectToSave)`  

**FETCHING FROM DB**  
Obviously, you need to get back your data.  
Use the Promise based `SAT.Utils.store.find` method.  
The find method resolves a promise on success and rejects it on error.  

Retrieve your data by specifying the "type" of object you want given back to you.   
- component  
`SAT.Utils.store.find("component", componentId)`  

- generic-element  
`SAT.Utils.store.find("generic-element", objectId)`  

- global-generic-element  
`SAT.Utils.store.find("global-generic-element", objectId)`  

**DELETING**  
Deleting data takes the same form.   
Use the Promise based `SAT.Utils.store.delete` method.  
The find method resolves a promise on success and rejects it on error.   

Again, this is just to show people in the community what can be done. We would strongly consider protecting those methods and making them more secure if you were to work on a commercial or very important project with that kind of feature exposed.  

- component  
`SAT.Utils.store.delete("component", componentId)`  

- generic-element  
`SAT.Utils.store.delete("generic-element", objectId)`  

- global-generic-element  
`SAT.Utils.store.delete("global-generic-element", objectId)`  


| Previous Page | Home | Next Page |
| ------ | ------ | ------ |
| [How to share components on the network.](./NetworkedComponents.md) | [index](../README.md) | [How to use communication channels.](./UsingCommChannels.md)  |

 