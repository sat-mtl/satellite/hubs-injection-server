# Setting a working local environment
  
As previously mentionned in this wiki, local environments have become quite tedious to work with over https due to added domain restrictions and tighter SSL certificate validation.  

We have finally come up with a solution that works on all environments and all browsers (except Firefox on Windows).  To put this into place you will need:  

* A local Mozilla Hubs Instance 
* A Hubs Injection server (if you are using our injection method) 
* On OSX -> You will need Homebrew installed. ( https://brew.sh/ ) This might take a while to install due to OSX requirements.  
* On Windows -> You will need Chocolatey (https://chocolatey.org/install) (and optionnaly Scoop (https://scoop.sh/) ) installed. 
* MKCert and NSS installed on your computer. Follow the following instructions:  https://github.com/FiloSottile/mkcert.  

* Nginx installed on your computer:  

Windows: You can install with scoop or chocolatey or do it manually by downloading the latest main line binary for windows https://nginx.org/en/download.html and follow the windows installation method here: https://www.nginx.com/resources/wiki/start/topics/tutorials/install/

Linux:  
The simplest way to install is:  
```
sudo apt update
sudo apt install nginx.
```

More details can be found here: https://www.nginx.com/resources/wiki/start/topics/tutorials/install/. 
  
OSX:  
Install nginx with homebrew
Details are here (you might need to update homebrew and/or OSX ) https://medium.com/@ThomasTan/installing-nginx-in-mac-os-x-maverick-with-homebrew-d8867b7e8a5a.  


## Create certificates and keys and create configuration files.

* Spawn up a shell, terminal or whatever command line utility you are using on your computer. 
* CD into the directory where MKCert was unzipped.  
* We first need to create a local root certificate by using the command :  
`mkcert -install` 

* Now we will create certificates and keys for both the injection server and our Hubs Client.  
First our Hubs Client, type:  
`mkcert hubs.local` 
  
* Then our injection server, type:  
`mkcert api.hubs.local`

* Once the certificates are built, you will need to locate them. Use the command line once more and type: 
`mkcert -CAROOT`  

This will output the location of your newly created certificates and keys.  
Go to that folder and rename the certificate named *hubs.local.pem* to *cert.pem* and rename the key named *hubs.local-key.pem to key.pem* .  
Copy those two renamed files to your clipboard and copy them at the root of your Hubs Client project in a folder called: certs (if one already exists, just paste-overwrite the files already in there).  

While we are in the Hubs Client, we will check on the .env file and make sure it is ready to use the injection server and the certificates. It should be located at the root of the project. The 3 following lines should be there: 
```
INJECTION_URL = https://api.hubs.local
CORS_PROXY_SERVER="hubs-proxy.com"
USE_LOCAL_SSL_CERTIFICATES = 1
```

If no .env file is present, go ahead and create one and insert the 3 lines shown above.  

**SAVE YOUR FILE**



Go back to the folder where mkcert created your certificates, and copy the *api.hubs.local-key.pem* and *api.hubs.local.pem* to your clipboard. You will then need to paste those two files in a location where nginx can find them easily. Best way to do it is to simply paste them in a folder inside the nginx conf folder. The nginx conf folder is usually located at:  

```
OSX: /usr/local/etc/nginx/
Ubuntu: /etc/nginx 
Windows: c:\nginx-yourVersion\conf\ 
```

In that folder you should have a few files, such as nginx.conf and nginx.conf.default . 
Inside the nginx conf folder, create another folder called *ssl* and paste your key and certifcate in there.  

Now we need to build a configuration for our nginx reverse-proxy server to redirect (proxy-pass) all communications directed to apu.hubs.local to our injection server located at 127.0.0.1:8600 (localhost:8600).  

In the nginx conf folder, locate the nginx.conf file and open it with admin rights (sudo nano nginx.conf ? ) and paste the following: 

```
events {}
http {
server {
    server_name api.hubs.local;

    location / {
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
        proxy_set_header   X-Forwarded-For $remote_addr;
        proxy_set_header   Host $http_host;
        proxy_pass         http://127.0.0.1:8600;
    }

    listen 443 ssl;
    ssl_certificate      ssl/api.hubs.local.pem;
    ssl_certificate_key  ssl/api.hubs.local-key.pem;
}
}
```

Save it and check the validity of this conf file by running : nginx -t (sudo nginx -t) in the command line. You should have a message telling you the configuration is ok. If not, check the file and make sure it is exactly what is written above.   

Reboot nginx services/server  
OSX: `sudo nginx` 
Linux: `sudo systemctl reload nginx` or `sudo systemctl restart nginx`. 
Windows: Double clicking nginx.exe should do it. But if it does not use:  
```
cd /nginx
taskkill /f /IM nginx.exe
start nginx
```

## Create hostnames to redirect traffic from hubs.local and api.hubs.local to localhost. 
SSL does not fare well with localhost, the key to this solution is to spoof traffic stemming from our hubs.local domain and our api.hubs.local subdomain to use 127.0.0.1 (localhost).  We do this by adding hostnames to our computer.  


OSX and Linux:  
With the command line, do: 
`sudo nano /etc/hosts` and add those two lines

```
127.0.0.1   hubs.local
127.0.0.1   api.hubs.local
```

Windows:  
Press the Windows key.  
Type Notepad in the search field.  
In the search results, right-click Notepad and select Run as administrator.  
From Notepad, open the following file:  
c:\Windows\System32\Drivers\etc\hosts. 
Add the lines:  
```
127.0.0.1   hubs.local
127.0.0.1   api.hubs.local
```

Select File > Save to save your changes.   
  
## Testing our apps with https. 
Let's test our apps with a browser.  
* First, reboot nginx again.  
* Start the injection server (I guess you would do a simple `node index.js` from the injection server's root.) 
* Open up your browser of choice (except firefox on windows).  
* Go to: `https://api.hubs.local` . The first time you try to connect, the browser might pop-up a security concern modal. Go to advanced and accept the certificate (always allow). Once connected, you should see an empty page with a small message that says: cannot get /  
If you do not see that message, go back through the steps and double-check your configuration files.  
* Once the injection server is running and the browser can connect to it, we can boot-up and test our Hubs Client.  Go to your Hubs Client project and run a `npm run dev` .  
* Once the build is complete, go back to your browser and type https://hubs.local:8080/ (you might get the same kind of security concern we got with the injection server api, just allow it in the advanced options ).  
* Create a room  
*. Open up the browser's developper console and check for connection errors to api.hubs.local, if you do not see anything of the sort, you have successfully setup a local environment for Mozilla Hubs and SAT's injection server.  





