class RectangleComponent {
    constructor(rectangleID, rectangleConfigs) {
        this.rectangleID = rectangleID;
        this.defaultConfigs =  {
            text_string:"DEFAULT TEXT",
            rectangle_width:2.5,
            rectangle_height:0.4,
            rectangle_color:'#550000',
            rectangle_position:'0 2 0',
        }

        this.configs = {...this.defaultConfigs, ...rectangleConfigs};
        this.registerComponent(this.configs);
        this.buildTemplate("rectangle-media");
        this.createEntity();
    }

    registerComponent(configs) {
        AFRAME.registerComponent('rectanglecomponent', {
            schema: {
                rectangle_text_string: { type:'string', default:configs.text_string },
                rectangle_color: { type: 'color', default: configs.rectangle_color },
                rectangle_id:{type:'string', default: this.rectangleID},
                rectangle_width:{type:'number', default: configs.rectangle_width },
                rectangle_height:{type:'number', default: configs.rectangle_height },
                rectangle_position:{type:'string', default:configs.rectangle_position},
            },
            init:function() {
                let data = this.data;
                let el = this.el;
                this.update = this.update.bind(this);
                this.geometry = new THREE.PlaneBufferGeometry(data.rectangle_width, data.rectangle_height, 1, 1);
                this.material = new THREE.MeshBasicMaterial({ color: data.rectangle_color });
                this.material.transparent = true;
                this.material.opacity = 0.8;
                this.material.side = THREE.DoubleSide;
                
                // Create mesh.
                this.mesh = new THREE.Mesh(this.geometry, this.material);
                // Set mesh on entity.
                el.setObject3D('mesh', this.mesh);
                el.setAttribute('position', `${data.rectangle_position}`);
                el.setAttribute('text', 'value', `${data.rectangle_text_string}`);
                el.setAttribute('text', 'align', 'center');
                el.setAttribute('text', 'transparent', 'false');
                el.setAttribute('text', 'wrapCount', 30);
                el.setAttribute('text', 'width', 2.5);
            }
        });
    }
    buildTemplate(templateID) {
        let assets = document.querySelector("a-assets");
        let newTemplate = document.createElement("template");
        
        newTemplate.id = `${templateID}`;
        newTemplate.innerHTML = `<a-entity 
        class="interactable" 
        is-remote-hover-target 
        set-unowned-body-kinematic 
        tags="isHandCollisionTarget: false; isHoldable: false; offersHandConstraint: false; offersRemoteConstraint: false; inspectable: true; singleActionButton:true; isStatic: true;togglesHoveredActionSet: true;" 
        body-helper="type: static; mass: 1; collisionFilterGroup: 1; collisionFilterMask: 15;"
        hoverable-visuals 
        matrix-auto-update
        pinnable
        rectanglecomponent
        >
        </a-entity>`;
        assets.appendChild(newTemplate);
        NAF.schemas.add({
            // template to add (created above)
            template: `#${templateID}`,
            components: [{
                    component: "position"
                },
                {
                    component: "rotation"
                },
                {
                    component: "scale"
                },
            ]
        });
    }

    createEntity() {
        return new Promise((resolve, reject) => { 
            let doCreate = async () => { 
                let rectangleEntity = document.createElement("a-entity");
                let loadedRectangle = new Promise((resolve, reject) => { rectangleEntity.addEventListener('loaded', resolve, { once: true }) });
                rectangleEntity.setAttribute("id", `${this.rectangleID}Entity`);
                rectangleEntity.setAttribute("rectanglecomponent", `rectangle_id:${this.rectangleID};`);
                document.querySelector('a-scene').appendChild(rectangleEntity);
                await loadedRectangle;
                
                return resolve(loadedRectangle);
            }
            doCreate();
        });
    }
}

let rectangle;
setTimeout(()=> {
    rectangle = new RectangleComponent("rectangle1", null);
}, 
1000);